package com.dev4j.springmetrics.controllers;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dev4j/metrics")
public class TestMetrics {

    private static final Logger logger = LoggerFactory.getLogger(TestMetrics.class);

    @Autowired
    private MeterRegistry registry;

    @GetMapping
    @Timed("devs4j.timer") // Tambien se puede poner a nivel de clases
    public ResponseEntity<String> get() {
        logger.info("MeterRegistry used {}", registry.getClass().getName());
        return new ResponseEntity<>("@raidentrance", HttpStatus.OK);
    }

}
